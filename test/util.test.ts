import { answer } from '../src/util';

describe('util tests', () => {
  test('Fetch task (200 OK)', () => {
    expect(answer()).toEqual(42);
  });
});
